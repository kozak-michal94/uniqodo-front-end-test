/* Voucher service */

const fetchPromoCode = (promoId, callback) => (
    fetch(`${api}/generate/${promoId}`)
        .then(response => response.json())
        .catch(e => console.error("ubq fetching error: ", e))
);

export default fetchPromoCode;

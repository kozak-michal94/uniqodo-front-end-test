# Embeddable Code Plugin

> Provided build in *./dist/ecp.js* is a production build with configured API endpoint to *https://codes.uniqodo.com*

## Installation

- Instalation require npm
- First install dependencies ```npm i --silent```

## Development build & visible example

To run dev server and development build run code below in bash (or another tool where you can access npm)

```shell
npm run dev
```

## Production build

To run production build run code below in bash (or another tool where you can access npm)
Please replace domain with your API endpoint (replace this with domain only) 
<DOMAIN> = https://codes.uniqodo.com

```shell
npm run build -- --define 'api="<DOMAIN>"'
```


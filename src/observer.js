/* Observer for new placeholders */

import { locate } from "./placeholders.js";

/**
 * Mutations handling
 * @param {MutationRecord[]} mutations Each mutation
 * @param {function} listener Listener which will be called when new promo is found
 * */
const handleMutations = (mutations, listener) => {
    for (let i = 0; i < mutations.length; i++)
        // Listen only for new elements
        if (mutations[i].addedNodes.length) {
            mutations[i].addedNodes.forEach(
                node => {
                    if (node.nodeType === 1) {
                        if (typeof node.dataset.promoid !== "undefined")
                            return listener(node);
                        const containedVouchers = locate(node);
                        if (containedVouchers.length)
                            containedVouchers.forEach(listener);
                    }
                }
            );
        }
};

/**
 * Watcher for new placeholders on the page using mutation observer only
 * @return {function}
 * */
export default (() => {
    const MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

    return (obj, listener) => {
        if (!obj || !obj.nodeType === 1)
            return;

        // Run only when mutation observer exists
        if (MutationObserver) {
            const obs = new MutationObserver(mutations => handleMutations(mutations, listener));
            obs.observe(obj, { 
                childList: true, 
                attributes: false,
                characterData: false,
                subtree: true
            });
        }
    }
})();

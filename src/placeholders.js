/* Placeholder manipulation */

import fetchPromoCode from "./voucherService.js";

/**
 * Locate placeholders on the page
 * @param {DOMNode} node Start element for search
 * @return {NodeList} 
 */
export const locate = (node = document.body) => (node.querySelectorAll("*[data-promoId]"));

/**
 * Ask API for promo and modify node
 * @param {DOMNode} node Placeholder containing promoid in data attribute
 */
export const processPlaceholder = node => fetchPromoCode(node.dataset.promoid)
    .then((response = {errors: []}) => {
        if (response.result === "success")
            return node.innerHTML = response.data.code;
        if (!response.errors.length)
            return; 
        node.innerHTML = response.errors.map(e => e.message).join(", ");
    });


export default { locate, processPlaceholder };

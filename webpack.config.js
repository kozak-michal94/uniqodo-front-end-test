/* Webpack conf */
const path = require("path");

const mockCorrect = {
    data: {
        code: "W8R3TTNR",
        discount: 40,
    },
    errors: [],
    result: "success"
};
const mockIncorrect = {
    data: null,
    errors: [{
        code: 304,
        message: "Promotion not found",
    }],
    result: "failure"
};

module.exports = {
    entry: "./src/index.js",
    // generate fole to ./dist/ecp.js
    output: {
        filename: "ecp.js",
        path: path.resolve(__dirname, "dist")
    },
    // Set up dev server
    devServer: {
        // Accessible directories
        contentBase: [
            path.join(__dirname, "example"),
            path.join(__dirname, "dist")
        ],
        // Simple mock
        before: app => {
            app.get("/generate/:promoId", 
                (req, res) => res
                    .set("Access-Control-Allow-Origin", "*")
                    .json(req.params.promoId === "err" ? mockIncorrect : mockCorrect)
            );
        },
        compress: true,
        port: 9000
    } 
};

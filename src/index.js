/* Bootstrap file */

import placeholder from "./placeholders.js";
import observer from "./observer.js";

const initialize = () => {
    // Find already presented placeholders
    placeholder.locate().forEach(
        node => placeholder.processPlaceholder(node)
    );

    // Observe for new placeholders
    observer(document.body, placeholder.processPlaceholder);
};

if (document.readyState === "complete" || document.readyState === "loaded")
    initialize();
else
    document.addEventListener("DOMContentLoaded", initialize); 

